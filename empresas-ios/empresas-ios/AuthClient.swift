//
//  AuthClient.swift
//  empresas-ios
//
//  Created by Adil Coelho on 10/12/21.
//

import Foundation
import ComposableArchitecture

public struct AuthClient {
    public var login: () -> Effect<Bool, Error>
    
    public var email: () -> String?
    public var password: () -> String?
    
    public struct Error: Swift.Error, Equatable {
        public init() {
            
        }
    }
}
