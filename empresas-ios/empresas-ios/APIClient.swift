//
//  APIClient.swift
//  empresas-ios
//
//  Created by Adil Coelho on 16/12/21.
//

import Foundation



struct AuthResponse: Codable {
    var success: Bool
}

enum APIError: Error {
        case invalidURL
        case missingData
    
    }

struct APIClient {
    
    
    
    struct Credentials: Codable {
        var email: String
        var password: String
    }
    
    static func login(email: String, password: String) async throws -> AuthResponse {
        guard let url = URL(string:"https://empresas.ioasys.com.br/api/v1/users/auth/sign_in") else {
            throw APIError.invalidURL
        }
        
        let credentials = Credentials(email: email, password: password)
        
        
        let body = try JSONEncoder().encode(credentials)
        print(body)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = body
        let (data, something) = try await URLSession.shared.data(for: request)
        print(something)
        let result = try JSONDecoder().decode(AuthResponse.self, from: data)
        
        return result
    }
    
    
    
    
}
