//
//  RootCore.swift
//  empresas-ios
//
//  Created by Adil Coelho on 09/12/21.
//

import Foundation
import ComposableArchitecture

struct RootState: Equatable{
    var login: LoginState?
}

enum RootAction: Equatable {
    case login(LoginAction)
}

struct RootEnvironment {
    
}

let rootReducer = Reducer<RootState, RootAction, RootEnvironment>.combine(
    loginReducer.optional().pullback(
        state: \.login, action: /RootAction.login, environment: { _ in LoginEnvironment() }
    ),
//    homeReducer.optional().pullback(
//        state: \.home, action: /RootAction.home, environment: { _ in HomeEnvironment() }
//    ),
    Reducer { state, action, environment in
        switch action {
        case .login(.loginResponse(.success(true))):
            state.login = nil
            return .none
        case .login:
            return .none
        }
    }
)

