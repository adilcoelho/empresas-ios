//
//  LoginView.swift
//  empresas-ios
//
//  Created by Adil Coelho on 08/12/21.
//

import SwiftUI
import ComposableArchitecture

struct LoginView: View {
    let store: Store<LoginState, LoginAction>
    
    var body: some View {
        WithViewStore(store) { viewStore in
            VStack {
                VStack {
                    Text("Digite seus dados para continuar")
                        .font(.body)
                    
                    TextField("Email", text: viewStore.binding(get: \.email, send: { LoginAction.binding(.set(\.$email, $0)) }))
                        .textFieldStyle(.roundedBorder)
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                        .frame(width: 327, height: 48, alignment: .center)
                        .padding(.horizontal, 24)
                        .disabled(viewStore.state.isSigningIn)
                    
                    SecureField("Senha", text: viewStore.binding(get: \.password, send: { LoginAction.binding(.set(\.$password, $0)) }))
                        .textFieldStyle(.roundedBorder)
                        .frame(width: 327, height: 48, alignment: .center)
                        .padding(.horizontal, 24)
                        .disabled(viewStore.state.isSigningIn)
                    
                    Button("ENTRAR") {
                        viewStore.send(.loginButtonTapped)
//                        Task {
//                            do {
//                                
//                                let albums = try await APIClient.login(email: viewStore.email, password: viewStore.password)
//                                print(albums)
//                                viewStore.send(.loginResponse(.success(true)))
//                                
//                            } catch {
//                                viewStore.send(.loginResponse(.failure(error as! APIError)))
//                            }
//                            
//                        }
                    }
                    .buttonStyle(.borderedProminent)
                }
            }
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        
        LoginView(store: Store<LoginState, LoginAction>(initialState: LoginState(email: "", password: ""), reducer: loginReducer, environment: LoginEnvironment()))
    }
}
