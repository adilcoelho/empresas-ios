//
//  Splash.swift
//  empresas-ios
//
//  Created by Adil Coelho on 08/12/21.
//

import SwiftUI
import ComposableArchitecture

struct Splash: View {
    let store: Store<RootState, RootAction>
    var body: some View {
        WithViewStore(store) { viewStore in
            VStack {
                
            }
            
        }
        
        
        
    }
}

struct Splash_Previews: PreviewProvider {
    static var previews: some View {
        Splash(store: Store(initialState: RootState(), reducer: rootReducer, environment: RootEnvironment()))
    }
}
