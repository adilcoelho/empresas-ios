//
//  LoadingView.swift
//  empresas-ios
//
//  Created by Adil Coelho on 08/12/21.
//

import SwiftUI

struct LoadingView: View {
    var body: some View {
        ProgressView()
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView()
    }
}
