//
//  RootView.swift
//  empresas-ios
//
//  Created by Adil Coelho on 14/12/21.
//

import SwiftUI
import ComposableArchitecture

struct RootView: View {
    var store: Store<RootState, RootAction>
    
    @ViewBuilder var body: some View {
        IfLetStore(store.scope(state: \.login, action: RootAction.login)) { store in
            LoginView(store: store)
        } else: {
            HomeView()
        }
        
    }
}
