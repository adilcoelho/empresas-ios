//
//  HomeCore.swift
//  empresas-ios
//
//  Created by Adil Coelho on 14/12/21.
//

import Foundation
import ComposableArchitecture


struct HomeState {
    var searchText: String = ""
    var companies: [Company]?
}

enum HomeAction: Equatable {
    
}

struct HomeEnvironment {
    
}

var homeReducer = Reducer<HomeState, HomeAction, HomeEnvironment> { state, action, environment in
    switch action {
        
    }
    
    return .none
}
