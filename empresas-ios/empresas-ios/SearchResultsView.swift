//
//  SearchResultsView.swift
//  empresas-ios
//
//  Created by Adil Coelho on 09/12/21.
//

import SwiftUI

struct SearchResultsView: View {
    let companies: [Company]
    
    let columns = [
        GridItem(.fixed(150)),
        GridItem(.fixed(150))
    ]
    
    var body: some View {
        LazyVGrid(columns: columns) {
            ForEach(companies) { company in
                NavigationLink(destination: DetailView(company: company)) {
                    SearchResultCard(company: company) {
                        RoundedRectangle(cornerRadius: 10)
                            .frame(width: 88, height: 128, alignment: .center)
                            .foregroundColor(.pink)
                    }
                    
                }
                .padding(.top)
            }
        }
    }
}

struct SearchResultCard<Content: View>: View {
    let company: Company
    let content: Content
    
    init(company: Company, @ViewBuilder content: @escaping () -> Content) {
        self.content = content()
        self.company = company
    }
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 10)
                .foregroundColor(.purple)
                .frame(width: 152, height: 107, alignment: .center)
            VStack {
                content
                Text(company.title)
                    .frame(width: 152, alignment: .center)
            }
        }
        
        
    }
}



struct Company: Identifiable {
    var id: UUID
    var image: Image = Image(systemName: "arrow")
    var title: String
}

struct SearchResultsView_Previews: PreviewProvider {
    static var previews: some View {
        SearchResultsView(companies: [
            Company(id: UUID(), title: "Company 1 "),
            Company(id: UUID(), title: "Another Company"),
            Company(id: UUID(), title: "McDonald's"),
        ])
    }
}
