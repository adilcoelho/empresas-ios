//
//  HomeView.swift
//  empresas-ios
//
//  Created by Adil Coelho on 08/12/21.
//

import SwiftUI
import ComposableArchitecture

struct HomeView: View {
//    var store: Store<HomeState, HomeAction>
    
    @State var searchText: String = ""
    var isSearching: Bool {
        !searchText.isEmpty
    }
    
    @State var companies: [Company]?
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                if (!isSearching) {
                    Text("Pesquise por uma empresa")
                        .font(.system(size: 40))
                        .fontWeight(.bold)
                        .frame(alignment: .topLeading)
                }
                    
                HStack {
                    if (!isSearching) {
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(.gray)
                            .padding(.leading)
                    }
                    
                    TextField("Buscar...", text: $searchText)
                        .padding(.leading)
                        .onSubmit {
                            companies = [
                                Company(id: UUID(), title: "Company 1 "),
                                Company(id: UUID(), title: "Another Company"),
                                Company(id: UUID(), title: "McDonald's"),
                            ]
                        }
                    
                }
                .frame(height: 48, alignment: .center)
                .overlay {
                    RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(.gray)
                }
                if (isSearching) {
                    if let companies = companies {
                        SearchResultsView(
                            companies: companies
                        )
                        .padding(.top)
                    }
                    
                }
                Spacer()
            }
            .padding(24)
            .toolbar {
                ToolbarItemGroup(placement: .navigationBarLeading) {
                    if (isSearching) {
                        Button(action: {
                            searchText = ""
                        }, label: {
                            Image(systemName: "arrow.backward")
                                .foregroundColor(.purple)
                        })
                    }
                }
            }
            .toolbar {
                ToolbarItemGroup(placement: .principal) {
                    if (isSearching) {
                        Text("Pesquise")
                            .font(.system(size: 24))
                            .fontWeight(.bold)
                    }
                }
            }
            
        }
        
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
