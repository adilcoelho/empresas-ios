//
//  empresas_iosApp.swift
//  empresas-ios
//
//  Created by Adil Coelho on 08/12/21.
//

import SwiftUI
import ComposableArchitecture

@main
struct empresas_iosApp: App {
    var body: some Scene {
        WindowGroup {
            RootView(store: Store<RootState, RootAction>(initialState: RootState(login: LoginState(email: "", password: "")), reducer: rootReducer, environment: RootEnvironment()))
        }
    }
}
