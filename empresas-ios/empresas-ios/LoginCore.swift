//
//  LoginCore.swift
//  empresas-ios
//
//  Created by Adil Coelho on 10/12/21.
//

import Foundation
import ComposableArchitecture

enum LoginFocus {
    case email
    case password
}

struct LoginState: Equatable {
    @BindableState var email: String
    @BindableState var password: String
    var isSigningIn: Bool = false
    
}

enum LoginAction: Equatable, BindableAction {
    case loginButtonTapped
    case loginResponse(Result<Bool, APIError>)
    case binding(BindingAction<LoginState>)
}


struct LoginEnvironment {
//    var authClient: AuthClient?
//    var mainQueue: ()? -> AnySchedulerOf<DispatchQueue>
}

let loginReducer = Reducer<LoginState, LoginAction, LoginEnvironment> { state, action, environment in
    switch action {
        
    case .loginButtonTapped:
        state.isSigningIn = true
        return Effect(value: .loginResponse(.success(true)))
    case .loginResponse(.failure(let error)):
        state.isSigningIn = false
        return .none
    case .loginResponse(_):
        return .none
    case .binding:
        return .none
    }
}
.binding()
