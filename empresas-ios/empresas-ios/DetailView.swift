//
//  DetailView.swift
//  empresas-ios
//
//  Created by Adil Coelho on 08/12/21.
//

import SwiftUI

struct DetailView: View {
    var company: Company
    var body: some View {
        Text("Company name: \(company.title)")
    }
}

//struct DetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        DetailView("test company")
//    }
//}
